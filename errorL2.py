from parameter import *
from readfile import preProcessData, conditionLimite, assembleMatriceConnection2D, extractPreProcessData
from femClass import Fem, Mesh
from commun import resolveproblem, os
from natsort import os_sorted
from math import *
import numpy as np
import matplotlib.pyplot as plt
P = 1000*10**6
MU = 0.3
E = 70000*10**6
COEF4 = (P*INTERN_RADIUS**2)/(EXTERN_RADIUS**2 - INTERN_RADIUS**2)

def solutionExactDisplacement(x, y):
  r = sqrt(x**2+y**2)
  sol = (r*P*INTERN_RADIUS**2)*((1-MU)+((1+MU)*(EXTERN_RADIUS**2))/(r**2))/(E*(EXTERN_RADIUS**2 - INTERN_RADIUS**2))
  return sol

def solutionExactRadialStress(x, y):
  r = sqrt(x**2+y**2)
  sol = COEF4*(1-(EXTERN_RADIUS/r)**2)
  return sol

def solutionExactTangentialStress(x, y):
  r = sqrt(x**2+y**2)
  sol = COEF4*(1+(EXTERN_RADIUS/r)**2)
  return sol

def processFem(filename, meshType):
  preProcessData(filename)
  node, connection, convection, dirichletX, dirichletY = extractPreProcessData()
  matriceConnection, temp = assembleMatriceConnection2D(connection, convection)
  dirichletX, dirichletY = conditionLimite(dirichletX, dirichletY)
  totalNumberOfNode = node.shape[0]
  numberOfElement = connection.shape[0]
  numberOfBorderElement = convection.shape[0]

  mesh = Mesh(node,temp,matriceConnection,numberOfElement,numberOfBorderElement,False)
  ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
  fem = Fem(mesh, ndlt, dirichletX, dirichletY, meshType, True)
  fem =resolveproblem(fem)

  return fem


def computeXandYT6(fem, ig, coor, u, v, sigmar, sigmat):
  N = fem.shapeFunction2D(fem.ksi[ig],fem.eta[ig])
  x = N[0]*coor[0,0] + N[1]*coor[1,0] + N[2]*coor[2,0] + N[3]*coor[3,0] + N[4]*coor[4,0] + N[5]*coor[5,0]
  y = N[0]*coor[0,1] + N[1]*coor[1,1] + N[2]*coor[2,1] + N[3]*coor[3,1] + N[4]*coor[4,1] + N[5]*coor[5,1]
  ug = N[0]*u[0] + N[1]*u[1] + N[2]*u[2] + N[3]*u[3] + N[4]*u[4] + N[5]*u[5]
  vg = N[0]*v[0] + N[1]*v[1] + N[2]*v[2] + N[3]*v[3] + N[4]*v[4] + N[5]*v[5]
  sigR = N[0]*sigmar[0] + N[1]*sigmar[1] + N[2]*sigmar[2] + N[3]*sigmar[3] + N[4]*sigmar[4] + N[5]*sigmar[5] 
  sigT = N[0]*sigmat[0] + N[1]*sigmat[1] + N[2]*sigmat[2] + N[3]*sigmat[3] + N[4]*sigmat[4] + N[5]*sigmat[5]

  return x, y, ug, vg, sigR , sigT

def calculErrorGaussT6(fem):
  nd = np.zeros(fem.numberOfNodePerElement, dtype=np.int32)
  coor = np.zeros((fem.numberOfNodePerElement, 2), dtype=np.float64)
  u= np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  v = np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  sigmar = np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  sigmat = np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  fem.gauss2D()
  errD = 0
  errR =  0
  errT = 0

  for e in range(fem.mesh.numberOfElement):
    coor,nd = fem.computeCoordonate(e)

    for i in range(0,fem.numberOfNodePerElement):
      u[i] = fem.u[nd[i]]
      v[i] = fem.v[nd[i]]
      sigmar[i] = fem.sigmaR[nd[i]]
      sigmat[i] = fem.sigmaTheta[nd[i]]

    for ig in range(0,fem.gaussPointNumber):
      x, y, ug, vg, sigR, sigT = computeXandYT6(fem, ig, coor, u, v, sigmar, sigmat)
      d =solutionExactDisplacement(x, y)
      r = solutionExactRadialStress(x, y)
      t = solutionExactTangentialStress(x, y)
      fem.computeJacobian(ig, coor)
      uvg = sqrt(ug**2+vg**2)
      errD += fem.determinantJacobian*fem.w[ig]*(uvg-d)**2
      errR += fem.determinantJacobian*fem.w[ig]*(sigR-r)**2
      errT += fem.determinantJacobian*fem.w[ig]*(sigT-t)**2
  
  errD = sqrt(errD)
  errR = sqrt(errR)
  errT = sqrt(errT)

  return errD, errR, errT

def errorL2T6():
  filenameList = os_sorted(os.listdir("../maillage/TrianglesL2"))
  errorDisplacement = []
  errorRadialStress = []
  errorTangentialStress = []
  n = []
  for filename in filenameList :
    print(filename)
    fem = processFem("../maillage/TrianglesL2/"+filename, True)
    erD , erR, erT = calculErrorGaussT6(fem)
    errorDisplacement.append(erD)
    errorRadialStress.append(erR)
    errorTangentialStress.append(erT)
    n.append(fem.mesh.numberOfElement)

  for i in range (0,len(n)):
    errorDisplacement[i] = log10(errorDisplacement[i])
    errorRadialStress[i] = log10(errorRadialStress[i])
    errorTangentialStress[i] = log10(errorTangentialStress[i])
    n[i] = log10(n[i])
  
  tmp = np.polyfit(n,errorDisplacement,1)
  print("\nordre de convergence deplacement log10", -tmp[0])
  plt.figure(num='erreur deplacement en fonction du nombre d\'element')
  plt.plot(n,errorDisplacement,label="erreur deplacement")
  plt.legend()

  tmp = np.polyfit(n,errorRadialStress,1)
  print("ordre de convergence contrainte radiale log10", -tmp[0])
  plt.figure(num='erreur radiale en fonction du nombre d\'element')
  plt.plot(n,errorRadialStress,label="erreur contrainte radiale")
  plt.legend()

  tmp = np.polyfit(n,errorTangentialStress,1)
  print("ordre de convergence contrainte tangentiel log10", -tmp[0])
  plt.figure(num='erreur tangentiel en fonction du nombre d\'element')
  plt.plot(n,errorTangentialStress,label="erreur contrainte tangentiel")
  plt.legend()

  plt.show()


def computeXandYQ8(fem, ig, coor, u, v, sigmar, sigmat):
  N = fem.shapeFunction2D(fem.ksi[ig],fem.eta[ig])
  x = N[0]*coor[0,0] + N[1]*coor[1,0] + N[2]*coor[2,0] + N[3]*coor[3,0] + N[4]*coor[4,0] + N[5]*coor[5,0] +N[6]*coor[6,0] + N[7]*coor[7,0]
  y = N[0]*coor[0,1] + N[1]*coor[1,1] + N[2]*coor[2,1] + N[3]*coor[3,1] + N[4]*coor[4,1] + N[5]*coor[5,1] +N[6]*coor[6,1] + N[7]*coor[7,1]
  ug = N[0]*u[0] + N[1]*u[1] + N[2]*u[2] + N[3]*u[3] + N[4]*u[4] + N[5]*u[5] +N[6]*u[6] + N[7]*u[7]
  vg = N[0]*v[0] + N[1]*v[1] + N[2]*v[2] + N[3]*v[3] + N[4]*v[4] + N[5]*v[5] +N[6]*v[6] + N[7]*v[7]
  sigR = N[0]*sigmar[0] + N[1]*sigmar[1] + N[2]*sigmar[2] + N[3]*sigmar[3] + N[4]*sigmar[4] + N[5]*sigmar[5] + N[6]*sigmar[6] + N[7]*sigmar[7]
  sigT = N[0]*sigmat[0] + N[1]*sigmat[1] + N[2]*sigmat[2] + N[3]*sigmat[3] + N[4]*sigmat[4] + N[5]*sigmat[5] + N[6]*sigmat[6] + N[7]*sigmat[7]

  return x, y, ug, vg, sigR , sigT

def calculErrorGaussQ8(fem):
  nd = np.zeros(fem.numberOfNodePerElement, dtype=np.int32)
  coor = np.zeros((fem.numberOfNodePerElement, 2), dtype=np.float64)
  u= np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  v = np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  sigmar = np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  sigmat = np.zeros(fem.numberOfNodePerElement, dtype=np.float64)
  fem.gauss2D()
  errD = 0
  errR =  0
  errT = 0

  for e in range(fem.mesh.numberOfElement):
    coor,nd = fem.computeCoordonate(e)

    for i in range(0,fem.numberOfNodePerElement):
      u[i] = fem.u[nd[i]]
      v[i] = fem.v[nd[i]]
      sigmar[i] = fem.sigmaR[nd[i]]
      sigmat[i] = fem.sigmaTheta[nd[i]]

    for ig in range(0,fem.gaussPointNumber):
      x, y, ug, vg, sigR, sigT = computeXandYQ8(fem, ig, coor, u, v, sigmar, sigmat)
      d =solutionExactDisplacement(x, y)
      r = solutionExactRadialStress(x, y)
      t = solutionExactTangentialStress(x, y)
      fem.computeJacobian(ig, coor)
      uvg = sqrt(ug**2+vg**2)
      errD += fem.determinantJacobian*fem.w[ig]*(uvg-d)**2
      errR += fem.determinantJacobian*fem.w[ig]*(sigR-r)**2
      errT += fem.determinantJacobian*fem.w[ig]*(sigT-t)**2
  
  errD = sqrt(errD)
  errR = sqrt(errR)
  errT = sqrt(errT)

  return errD, errR, errT

def errorL2Q8():
  filenameList = os_sorted(os.listdir("../maillage/QuadranglesL2"))
  errorDisplacement = []
  errorRadialStress = []
  errorTangentialStress = []
  n = []
  for filename in filenameList :
    print(filename)
    fem = processFem("../maillage/QuadranglesL2/"+filename, False)
    erD , erR, erT = calculErrorGaussQ8(fem)
    errorDisplacement.append(erD)
    errorRadialStress.append(erR)
    errorTangentialStress.append(erT)
    n.append(fem.mesh.numberOfElement)


  for i in range (0,len(n)):
    errorDisplacement[i] = log10(errorDisplacement[i])
    errorRadialStress[i] = log10(errorRadialStress[i])
    errorTangentialStress[i] = log10(errorTangentialStress[i])
    n[i] = log10(n[i])
  
  tmp = np.polyfit(n,errorDisplacement,1)
  print("\nordre de convergence deplacement log10", -tmp[0])
  plt.figure(num='erreur deplacement en fonction du nombre d\'element')
  plt.plot(n,errorDisplacement,label="erreur deplacement")
  plt.legend()

  tmp = np.polyfit(n,errorRadialStress,1)
  print("ordre de convergence contrainte radiale log10", -tmp[0])
  plt.figure(num='erreur radiale en fonction du nombre d\'element')
  plt.plot(n,errorRadialStress,label="erreur contrainte radiale")
  plt.legend()

  tmp = np.polyfit(n,errorTangentialStress,1)
  print("ordre de convergence contrainte tangentiel log10", -tmp[0])
  plt.figure(num='erreur tangentiel en fonction du nombre d\'element')
  plt.plot(n,errorTangentialStress,label="erreur contrainte tangentiel")
  plt.legend()

  plt.show()



def processFem2(filename, meshType):
  preProcessData(filename)
  node, connection, convection, dirichletX, dirichletY = extractPreProcessData()
  matriceConnection, temp = assembleMatriceConnection2D(connection, convection)
  dirichletX, dirichletY = conditionLimite(dirichletX, dirichletY)
  totalNumberOfNode = node.shape[0]
  numberOfElement = connection.shape[0]
  numberOfBorderElement = convection.shape[0]

  mesh = Mesh(node,temp,matriceConnection,numberOfElement,numberOfBorderElement,False)
  ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
  fem = Fem(mesh, ndlt, dirichletX, dirichletY, meshType, False)
  fem =resolveproblem(fem)

  return fem