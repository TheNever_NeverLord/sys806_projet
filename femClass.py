from math import *
import numpy as np
import sys
from parameter import *
from commun import bcolors

class Mesh():
  numberFreedomDegreesPerNode = 2
  numberOfNodePerBorderElement = 3

  def __init__(self, node, matriceConnection,matriceExport, numberOfElement, numberOfBorderElement, meshType):
    self.nodes = node
    self.matriceExport = matriceExport
    self.matriceConnection = matriceConnection
    self.numberOfElement = numberOfElement
    self.numberOfBorderElement = numberOfBorderElement
    self.numberOfNodes = node.shape[0]

    if(meshType == True):
      self.numberOfNodePerElement = 6
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement
    else:
      self.numberOfNodePerElement = 8
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement

class Fem():
  numberFreedomDegreesPerNode = 2
  numberFreedomDegreesPerBorderNode = 2
  numberOfNodePerBorderElement = 3
  Nuic = numberFreedomDegreesPerBorderNode*numberOfNodePerBorderElement

  def __init__(self, mesh, ndlt, dirichletX, dirichletY, meshType, isStressElasticity):
    self.mesh = mesh                    
    self.ndlt = ndlt
    self.dirichletX = dirichletX
    self.dirichletY = dirichletY
    self.isT6 = meshType
    self.isStressElasticity = isStressElasticity
    self.jacobian = 0
    self.determinantJacobian = 0
    self.inverseJacobain = 0
    self.ksi, self.eta, self.w = 0, 0, 0
    self.ksi1D, self.w1D = 0, 0
    self.Flag = False

    if(self.isStressElasticity == True):
      self.E = 70000*10**6
      self.P = 1000*10**6
      self.MU = 0.3
      facteur = self.E/(1-self.MU**2)
      self.D = facteur*np.array([[1, self.MU, 0],[self.MU, 1, 0],[0, 0, (1-self.MU)/2]], dtype=np.float64)
    elif(self.isStressElasticity == False):
      self.E = 2.8*10**6
      self.P = 1000*10**6
      self.MU = 0.45
      facteur = self.E/((1-self.MU*2)*(1+self.MU))
      nu = self.E/(2+2*self.MU)
      coef = facteur*self.MU
      self.D1 = np.array([[2*nu, 0, 0],[0, 2*nu, 0],[0, 0, nu]], dtype=np.float64)
      self.D2 = coef*np.array([[1, 1, 0],[1, 1, 0],[0, 0, 0]], dtype=np.float64)
      self.D = self.D1 + self.D2
    else:
      sys.exit(f"{bcolors.FAIL}error value not expected for isStressElasticity in Fem constructor, expected True or False{bcolors.ENDC}")

    if(self.isT6 == True):
      self.gaussPointNumber = 12
      self.numberOfNodePerElement = 6
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement
      self.npg = 3
    elif(self.isT6 == False):
      self.gaussPointNumber = 9
      self.numberOfNodePerElement = 8
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement
      self.npg = 4
    else:
      sys.exit(f"{bcolors.FAIL}error value not expected for isT6 in Fem constructor, expected True or False{bcolors.ENDC}")

    self.k = np.zeros((self.ndlt, self.ndlt), dtype=np.float64)
    self.Kn = np.zeros((self.ndlt, self.ndlt), dtype=np.float64)
    self.f = np.zeros(self.ndlt, dtype=np.float64)
    self.Fn = np.zeros(self.ndlt, dtype=np.float64)
    self.r = np.zeros(self.ndlt, dtype=np.float64)
    self.m = np.zeros((self.mesh.numberOfNodes,self.mesh.numberOfNodes), dtype=np.float64)
    self.u = np.zeros(self.mesh.numberOfNodes, dtype=np.float64)
    self.v = np.zeros(self.mesh.numberOfNodes, dtype=np.float64)
    self.Gx = np.zeros(self.mesh.numberOfNodes, dtype=np.float64)
    self.Gy = np.zeros(self.mesh.numberOfNodes, dtype=np.float64)
    self.Gxy = np.zeros(self.mesh.numberOfNodes, dtype=np.float64)
    self.contraintes_n = np.zeros((3, self.mesh.numberOfNodes), dtype=np.float64) 
    self.solution = np.zeros(self.mesh.numberOfNodes, dtype=np.float64)

    self.gauss2D()
    

  def gauss1D(self,npg):
    if(npg ==1):
      self.ksi1D = np.array([0], dtype=np.float64)
      self.w1D = np.array([2], dtype=np.float64)
    elif(npg ==2):
      self.ksi1D = np.array([1/sqrt(3), -1/sqrt(3)], dtype=np.float64)
      self.w1D = np.array([1, 1], dtype=np.float64)
    elif(npg ==3):
      self.ksi1D = np.array([0, -sqrt(3/5.), sqrt(3/5.)], dtype=np.float64)
      self.w1D = np.array([8/9., 5/9., 5/9.], dtype=np.float64)
    elif(npg ==4):
      self.ksi1D = np.array([sqrt((3-2*sqrt(6/5.))/7.), -sqrt((3-2*sqrt(6/5.))/7.), sqrt((3+2*sqrt(6/5.))/7.), -sqrt((3+2*sqrt(6/5.))/7.)], dtype=np.float64)
      self.w1D = np.array([0.5+(1/6.)*1./sqrt(6/5.), 0.5+(1/6.)*1./sqrt(6/5.), 0.5-(1/6.)*1./sqrt(6/5.), 0.5-(1/6.)*1./sqrt(6/5.)], dtype=np.float64)
    else:
      sys.exit(f"{bcolors.FAIL}error value not expected, expected 1,2,3,4 in gauss integration 1D{bcolors.ENDC}")


  def gauss2D(self):
    if(self.isT6 == True and self.Flag == False):
      if(self.gaussPointNumber == 3):
        self.w = np.array([1/6, 1/6, 1/6], dtype=np.float64)
        self.ksi = np.array([1/6, 2/3, 1/6], dtype=np.float64)
        self.eta = np.array([1/6, 1/6, 2/3], dtype=np.float64)
      elif(self.gaussPointNumber == 4):
        self.w = np.array([-27/96, 25/96, 25/96, 25/96], dtype=np.float64)
        self.ksi = np.array([1/3, 1/5, 3/5, 1/5], dtype=np.float64)
        self.eta = np.array([1/3, 1/5, 1/5, 3/5], dtype=np.float64)
      elif(self.gaussPointNumber == 7):
        a = (6+sqrt(15))/21
        b = (4/7)-a
        w1 = (155+sqrt(15))/2400
        w2 = (31/240)-w1
        self.w = np.array([9/80, w1, w1, w1, w2, w2, w2], dtype=np.float64)
        self.ksi = np.array([1/3, a, 1-2*a, a, b, 1-2*b, b], dtype=np.float64)
        self.eta = np.array([1/3, a, a, 1-2*a, b, b, 1-2*b], dtype=np.float64)
      elif(self.gaussPointNumber == 12):
        a = 0.063089014491502
        b = 0.249286745170910
        c = 0.310352451033785
        d = 0.053145049844816
        w1 = 0.025422453185103
        w2 = 0.058393137863189
        w3 = 0.041425537809187
        self.w = np.array([w1, w1, w1, w2, w2, w2, w3, w3, w3, w3, w3, w3], dtype=np.float64)
        self.ksi = np.array([a, 1-2*a, a, b, 1-2*b, b, c, d, 1-c-d, 1-c-d, c, d], dtype=np.float64)
        self.eta = np.array([a, a, 1-2*a, b, b, 1-2*b, d, c, c, d, 1-c-d, 1-c-d], dtype=np.float64)
      else:
        sys.exit(f"{bcolors.FAIL}error value not expected T6, expected 3,4,7,12 in gauss integration 2D{bcolors.ENDC}")
    elif(self.isT6 == False and self.Flag == False):
      if(self.gaussPointNumber == 4):
        self.w = np.array([1, 1, 1, 1], dtype=np.float64)
        temp = 1./sqrt(3.)
        self.ksi = np.array([temp, temp, -temp, -temp], dtype=np.float64)
        self.eta = np.array([temp, -temp, -temp, temp], dtype=np.float64)
      elif(self.gaussPointNumber == 7):
        temp = 1./sqrt(3/5)
        self.w = np.array([8/7, 20/63, 20/63, 20/36, 20/36, 20/36, 20/36], dtype=np.float64)
        self.ksi = np.array([0, 0, 0, temp, -temp, -temp, temp], dtype=np.float64)
        self.eta = np.array([0, sqrt(14/15), -sqrt(14/15), temp, temp, -temp, -temp], dtype=np.float64)
      elif(self.gaussPointNumber == 9):
        w1 = np.array([8/9., 5/9., 5/9.], dtype=np.float64)
        x1 = np.array([0, -sqrt(3/5.), sqrt(3/5.)])
        self.w = np.zeros(9, dtype=np.float64)
        self.ksi = np.zeros(9, dtype=np.float64)
        self.eta = np.zeros(9, dtype=np.float64)
        k=0
        for i in range(0,3):
            for j in range(0,3):
                self.w[k] = w1[i]*w1[j]
                self.ksi[k] = x1[i]
                self.eta[k] = x1[j]
                k=k+1
      else:
        sys.exit(f"{bcolors.FAIL}error value not expected Q8, expected 4,7,9 in gauss integration 2D{bcolors.ENDC}")
    elif(self.isStressElasticity == False and self.Flag == True):
      if(self.isT6 == True):
        if(self.npg == 3):
          self.w = np.array([1/6, 1/6, 1/6], dtype=np.float64)
          self.ksi = np.array([1/6, 2/3, 1/6], dtype=np.float64)
          self.eta = np.array([1/6, 1/6, 2/3], dtype=np.float64)
          self.Flag = False
      elif(self.isT6 == False):
        if(self.npg == 4):
          self.w = np.array([1, 1, 1, 1], dtype=np.float64)
          temp = 1./sqrt(3.)
          self.ksi = np.array([temp, temp, -temp, -temp], dtype=np.float64)
          self.eta = np.array([temp, -temp, -temp, temp], dtype=np.float64)
          self.Flag = False
        else:
          sys.exit(f"{bcolors.FAIL}error value not expected for reduce Inegral, expected 3 for T6 or 4 for Q8 in gauss integration 2D{bcolors.ENDC}")

  def feeldof(self, nd, numberOfNodePerElement, numberFreedomDegreesPerNode):
    edof = numberOfNodePerElement*numberFreedomDegreesPerNode
    index = np.zeros(edof, dtype=np.int32)
    k = 0

    for i in range(0, numberOfNodePerElement):
      start = nd[i]*numberFreedomDegreesPerNode
      for j in range(0,numberFreedomDegreesPerNode):
        index[k] = start+j
        k = k+1

    return index
  
  def assemblekAndF(self, ke, fe, index):
    edof = len(index)

    for i in range(0, edof):
      i_index = index[i]
      for j in range(0, edof):
        j_index=index[j]
        self.k[i_index,j_index] = self.k[i_index,j_index]+ke[i,j]
      self.f[i_index] =  self.f[i_index]+fe[i]

  def assembleF(self, fe, index):
    edof = len(index)

    for i in range(0, edof):
      i_index = index[i]
      self.f[i_index] =  self.f[i_index]+fe[i]

  def assemblek(self, ke, index):
    edof = len(index)

    for i in range(0, edof):
      i_index = index[i]
      for j in range(0, edof):
        j_index=index[j]
        self.k[i_index,j_index] = self.k[i_index,j_index]+ke[i,j]

  def assembleM(self, me, index):
    edof = len(index)

    for i in range(0, edof):
      i_index = index[i]
      for j in range(0, edof):
        j_index=index[j]
        self.m[i_index,j_index]=self.m[i_index,j_index]+me[i,j]

  def computeCoordonate(self, ie):
    nd = np.zeros(self.numberOfNodePerElement, dtype=np.int32)
    coor = np.zeros((self.numberOfNodePerElement, 2), dtype=np.float64)

    for i in range(0, self.numberOfNodePerElement):
      nd[i] = self.mesh.matriceConnection[ie,i]
      coor[i,0] = self.mesh.nodes[nd[i],0]  
      coor[i,1] = self.mesh.nodes[nd[i],1]

    return coor, nd

  def BKsiEta(self, ksi, eta):
    if(self.isT6 == True):
      Bke = np.array([
        [4.0*eta+4.0*ksi-3.0, -4.0*eta-8.0*ksi+4.0, 4.0*ksi-1.0, 4.0*eta, 0, -4.0*eta],
        [4.0*eta+4.0*ksi-3.0, -4.0*ksi, 0, 4.0*ksi, 4.0*eta-1.0, -8.0*eta-4.0*ksi+4.0]],
        dtype=np.float64)
    else:
      Bke = np.array([
        [-eta**2/4-eta*ksi/2+eta/4+ksi/2,ksi*(eta-1), eta**2/4-eta*ksi/2-eta/4+ksi/2,1/2-eta**2/2, eta**2/4+eta*ksi/2+eta/4+ksi/2,-ksi*(eta+1), -eta**2/4+eta*ksi/2-eta/4+ksi/2, eta**2/2-1/2],
        [-eta*ksi/2+eta/2-ksi**2/4+ksi/4,ksi**2/2-1/2, eta*ksi/2+eta/2-ksi**2/4-ksi/4,-eta*(ksi+1), eta*ksi/2+eta/2+ksi**2/4+ksi/4,1/2-ksi**2/2, -eta*ksi/2+eta/2+ksi**2/4-ksi/4, eta*(ksi-1)]], 
        dtype=np.float64)

    return Bke

  def computeJacobian(self, i, coor):
    Bke = self.BKsiEta(self.ksi[i],self.eta[i])
    self.jacobian = np.matmul(Bke,coor)
    self.inverseJacobain = np.linalg.inv(self.jacobian) 
    self.determinantJacobian = np.linalg.det(self.jacobian)

    return Bke 

  def shapeFunction2D(self, ksi, eta):
    if(self.isT6 == True):
      N1  =  2.0*eta**2 + 4.0*eta*ksi - 3.0*eta + 2.0*ksi**2 - 3.0*ksi + 1
      N2  =  4.0*ksi*(-eta - ksi + 1)
      N3  =  ksi*(2.0*ksi - 1.0)
      N4  =  4.0*eta*ksi
      N5  =  eta*(2.0*eta - 1.0)
      N6  =  4.0*eta*(-eta - ksi + 1)

      return np.array([N1, N2, N3, N4, N5, N6], dtype=np.float64)
    else :
      N1  =  -eta**2*ksi/4 + eta**2/4 - eta*ksi**2/4 + eta*ksi/4 + ksi**2/4 - 1/4
      N2  =  eta*ksi**2/2 - eta/2 - ksi**2/2 + 1/2
      N3  =  eta**2*ksi/4 + eta**2/4 - eta*ksi**2/4 - eta*ksi/4 + ksi**2/4 - 1/4
      N4  =  -eta**2*ksi/2 - eta**2/2 + ksi/2 + 1/2
      N5  =  eta**2*ksi/4 + eta**2/4 + eta*ksi**2/4 + eta*ksi/4 + ksi**2/4 - 1/4
      N6  =  -eta*ksi**2/2 + eta/2 - ksi**2/2 + 1/2
      N7  =  -eta**2*ksi/4 + eta**2/4 + eta*ksi**2/4 - eta*ksi/4 + ksi**2/4 - 1/4
      N8  =  eta**2*ksi/2 - eta**2/2 - ksi/2 + 1/2

      return np.array([N1, N2, N3, N4, N5, N6, N7, N8], dtype=np.float64)

  def sortShapeGradient(self, Bref):
    B=np.zeros((3, self.numberOfNodePerElement*2), dtype=np.float64)
    counter, carry = 0, 0
    counterBar = 1

    for i in range(0,B.shape[1]):
      B[counter,i] = Bref[counter,carry]
      B[2,i] = Bref[counterBar,carry]
      counter = counter+1
      counterBar = counterBar+1
      if(counter == 2):
        carry = carry+1
      counter = counter%2
      counterBar = counterBar%2

    return B

  def sortShapeFunction2D(self, ig):
    N=np.zeros((2, self.numberOfNodePerElement*2), dtype=np.float64)
    f = self.shapeFunction2D(self.ksi[ig], self.eta[ig])
    counter, carry = 0,0

    for i in range(0, N.shape[1]):
      N[counter,i] = f[carry]
      counter = counter+1
      if(counter == 2):
        carry = carry+1
      counter = counter%2

    return N

  def digitalIntegralInternalNode(self, coor):
    ke = np.zeros((self.Nui, self.Nui), dtype=np.float64)
    fe = np.zeros(self.Nui, dtype=np.float64)
    me = np.zeros((self.numberOfNodePerElement, self.numberOfNodePerElement), dtype=np.float64) 
    self.gauss2D()
    
    for i in range(0, self.gaussPointNumber):
      N=np.zeros((2, self.numberOfNodePerElement*2), dtype=np.float64) 
      B = np.zeros((3, self.numberOfNodePerElement*2), dtype=np.float64) 
      forceVolumique=np.zeros(2, dtype=np.float64) 

      G = self.computeJacobian(i, coor)
      Bref = np.matmul(self.inverseJacobain, G)
      f = self.shapeFunction2D(self.ksi[i], self.eta[i])
      N = self.sortShapeFunction2D(i)
      B = self.sortShapeGradient(Bref)
      forceVolumique[0]= 0 # rho*fx
      forceVolumique[1]= 0 # rho*fy

      if(self.isStressElasticity == False): 
        ke = ke  + self.determinantJacobian*self.w[i]*np.linalg.multi_dot([B.transpose(), self.D1, B])
      else :
        ke = ke  + self.determinantJacobian*self.w[i]*np.linalg.multi_dot([B.transpose(), self.D, B])

      me = me + self.determinantJacobian*self.w[i]*np.tensordot(f.transpose(), f, 0)
      fe  = fe  + self.determinantJacobian*self.w[i]*N.transpose().dot(forceVolumique) 

    return ke,fe,me

  def digitalReduceIntegralInternalNode(self, coor):
    ke = np.zeros((self.Nui, self.Nui), dtype=np.float64)
    self.Flag = True
    self.gauss2D()

    for i in range(0, self.npg):
      B = np.zeros((3, self.numberOfNodePerElement*2), dtype=np.float64) 

      G = self.computeJacobian(i, coor)
      Bref = np.matmul(self.inverseJacobain, G)
      B = self.sortShapeGradient(Bref)

      ke = ke  + self.determinantJacobian*self.w[i]*np.linalg.multi_dot([B.transpose(), self.D2, B])

    return ke


  def assembleMatrixInternalNode(self, nd, ke, fe, me):
    Kloc = self.feeldof(nd, self.numberOfNodePerElement, self.numberFreedomDegreesPerNode)
    self.assemblekAndF(ke, fe, Kloc)

    Kloc2=self.feeldof(nd, self.numberOfNodePerElement, 1)
    self.assembleM(me, Kloc2)

  def computeInternalNodes(self):
    ke = np.zeros((self.Nui, self.Nui), dtype=np.float64)
    fe = np.zeros((self.Nui, self.Nui), dtype=np.float64)
    me = np.zeros((self.numberOfNodePerElement, self.numberOfNodePerElement), dtype=np.float64) 

    for ie in range(0, self.mesh.numberOfElement):
      nd = np.zeros(self.numberOfNodePerElement, dtype=np.int32)
      coor = np.zeros((self.numberOfNodePerElement, 2), dtype=np.float64)

      coor, nd =self.computeCoordonate(ie)
      ke, fe, me = self.digitalIntegralInternalNode(coor)
      self.assembleMatrixInternalNode(nd, ke, fe, me)

      if(self.isStressElasticity == False):
        ke = self.digitalReduceIntegralInternalNode(coor)
        kloc = self.feeldof(nd, self.numberOfNodePerElement, self.numberFreedomDegreesPerNode)
        self.assemblek(ke, kloc)
      
  def computeBorderCoordonate(self,ie):
    nd = np.zeros(self.numberOfNodePerBorderElement, dtype=np.int32)
    coor = np.zeros((self.numberOfNodePerBorderElement, 2), dtype=np.float64)

    for i in range(0, self.numberOfNodePerBorderElement):
      nd[i] = self.mesh.matriceConnection[ie,i]
      coor[i,0] = self.mesh.nodes[nd[i],0]  
      coor[i,1] = self.mesh.nodes[nd[i],1]

    return nd, coor

  def shapeFunction1D(self, ksi):
    N1 = ksi*(ksi-1)/2.
    N2 = -(ksi-1)*(ksi+1)
    N3 = ksi*(ksi+1)/2.

    return np.array([N1, N2, N3])

  def sortShapeFunction(self, ig):
    N=np.zeros((2, self.numberOfNodePerBorderElement*2), dtype=np.float64)
    f = self.shapeFunction1D(self.ksi1D[ig])
    counter, carry = 0,0

    for i in range(0,N.shape[1]):
      N[counter,i] = f[carry]
      counter = counter+1
      if(counter == 2):
        carry = carry+1
      counter = counter%2

    return N 

  def assembleMatrixBorderNode(self, nd, fe):
    Kloc = self.feeldof(nd, self.numberOfNodePerBorderElement, self.numberFreedomDegreesPerBorderNode)
    self.assembleF(fe, Kloc)

  def computeXandY(self,ig,coor):
    N = self.shapeFunction1D(self.ksi1D[ig])
    x = N[0]*coor[0,0] + N[1]*coor[1,0] + N[2]*coor[2,0]
    y = N[0]*coor[0,1] + N[1]*coor[1,1] + N[2]*coor[2,1]
    dx = coor[0,0]*(self.ksi1D[ig]-0.5) - 2*coor[1,0]*self.ksi1D[ig] + coor[2,0]*(self.ksi1D[ig]+0.5)
    dy = coor[0,1]*(self.ksi1D[ig]-0.5) - 2*coor[1,1]*self.ksi1D[ig] + coor[2,1]*(self.ksi1D[ig]+0.5)
    
    return x, y, dx, dy

  def computeCauchyBoundaryConditionNodes(self):
    npg=4
    self.gauss1D(npg) 
    for ie in range(self.mesh.numberOfElement, self.mesh.numberOfElement+self.mesh.numberOfBorderElement):
      fe = np.zeros(self.Nuic, dtype=np.float64)
      nd = np.zeros(self.numberOfNodePerBorderElement, dtype=np.int32)
      coor = np.zeros((self.numberOfNodePerBorderElement, 2), dtype=np.float64)

      nd, coor = self.computeBorderCoordonate(ie)

      for ig in range(0,npg):
        N=np.zeros((2, self.numberOfNodePerBorderElement*2), dtype=np.float64) 
        forceSurfacique=np.zeros(2, dtype=np.float64)

        N = self.sortShapeFunction(ig)
        x ,y, dx, dy = self.computeXandY(ig,coor)
        determinantJacobian = sqrt(dx**2+dy**2)
        forceSurfacique[0]=self.P*(x/INTERN_RADIUS)
        forceSurfacique[1]=self.P*(y/INTERN_RADIUS)
        fe = fe + determinantJacobian*self.w1D[ig]*N.transpose().dot(forceSurfacique)

      self.assembleMatrixBorderNode(nd, fe)
 
  def dirichletBoundaryConditions(self):
    self.Kn = self.k
    self.Fn = self.f

    for i in range(0, self.dirichletX.shape[0]):
      nn = self.dirichletX[i,0]
      nn = int(nn*self.numberFreedomDegreesPerNode)
      for j in range(0, self.ndlt):
        if j == nn:
          self.k[j,j] = 1.0
          self.f[nn] = self.dirichletX[i,1]
        else:
          self.k[nn,j] = 0.0

    for i in range(0, self.dirichletY.shape[0]):
      nn = self.dirichletY[i,0]
      nn = int(nn*self.numberFreedomDegreesPerNode+1)
      for j in range(0, self.ndlt):
        if j == nn:
          self.k[j,j] = 1.0
          self.f[nn] = self.dirichletY[i,1]
        else:
          self.k[nn,j] = 0.0

  def solve(self):
    self.solution = np.linalg.solve(self.k, self.f)

    for z in range(0, self.mesh.numberOfNodes):
      self.u[z] = self.solution[2*z]
      self.v[z] = self.solution[2*z+1]

  def computeResidue(self):
    self.r = self.Kn.dot(self.solution)-self.Fn
  
  def digitalIntegralConstraint(self, coor, uv):
    deform = np.zeros(3, dtype=np.float64) 
    constraint= np.zeros(3, dtype=np.float64)
    gex = np.zeros(self.numberOfNodePerElement, dtype=np.float64)
    gey = np.zeros(self.numberOfNodePerElement, dtype=np.float64)
    gexy = np.zeros(self.numberOfNodePerElement, dtype=np.float64) 
    self.gauss2D()
    for i in range (0, self.gaussPointNumber):
      B = np.zeros((3, self.numberOfNodePerElement*2), dtype=np.float64) 

      G = self.computeJacobian(i, coor)
      Bref = np.matmul(self.inverseJacobain, G)
      f = self.shapeFunction2D(self.ksi[i], self.eta[i])
      B = self.sortShapeGradient(Bref)
      deform=B.dot(uv) 
      constraint=self.D.dot(deform)  
        
      gex= gex+self.w[i]*f.transpose()*constraint[0]*self.determinantJacobian 
      gey= gey+ self.w[i]*f.transpose()*constraint[1]*self.determinantJacobian 
      gexy= gexy+self.w[i]*f.transpose()*constraint[2]*self.determinantJacobian 

    return gex, gey, gexy

  def assembleG(self, index, gex, gey, gexy):
    edof = len(index)

    for i in range(0, edof):
      i_index = index[i]
      self.Gx[i_index] =  self.Gx[i_index]+gex[i]
      self.Gy[i_index] =  self.Gy[i_index]+gey[i]
      self.Gxy[i_index] =  self.Gxy[i_index]+gexy[i]

  def assembleConstraint(self, nd, gex, gey, gexy):
    kloc = self.feeldof(nd, self.numberOfNodePerElement, 1)
    self.assembleG(kloc, gex, gey, gexy)

  def extractSolution(self,ie):
    uv = np.zeros(self.Nui, dtype=np.float64)
    nd = self.mesh.matriceConnection[ie,:]
    Kloc=self.feeldof(nd, self.numberOfNodePerElement, self.numberFreedomDegreesPerNode)

    for k in range(0, self.Nui):
      temp = Kloc[k]
      uv[k] = self.solution[temp]

    return uv

  def computeConstraint(self):
    uv = np.zeros(self.Nui, dtype=np.float64)
    gex = np.zeros(self.numberOfNodePerElement, dtype=np.float64)
    gey = np.zeros(self.numberOfNodePerElement, dtype=np.float64)
    gexy = np.zeros(self.numberOfNodePerElement, dtype=np.float64) 

    for ie in range(0, self.mesh.numberOfElement):
      nd = np.zeros(self.numberOfNodePerElement, dtype=np.int32)
      coor = np.zeros((self.numberOfNodePerElement, 2), dtype=np.float64)

      uv = self.extractSolution(ie)
      coor, nd = self.computeCoordonate(ie)
      gex, gey ,gexy = self.digitalIntegralConstraint(coor, uv)
      self.assembleConstraint(nd, gex, gey, gexy)
  
  def computePolarConstraint(self):
    self.sigmaR = []
    self.sigmaTheta = []
    self.sigmaRTheta =[]

    i=0
    for x,y,z in self.mesh.nodes:
      theta = atan2(y,x)
      temp = self.sigmax[i]*cos(theta)*cos(theta) + self.sigmay[i]*sin(theta)*sin(theta) + self.sigmaxy[i]*sin(2*theta)
      self.sigmaR.append(temp)

      temp = self.sigmay[i]*cos(theta)*cos(theta) + self.sigmax[i]*sin(theta)*sin(theta) - self.sigmaxy[i]*sin(2*theta)
      self.sigmaTheta.append(temp)

      temp = cos(theta)*sin(theta)*(self.sigmay[i] - self.sigmax[i]) + self.sigmaxy[i]*cos(2*theta)
      self.sigmaRTheta.append(temp)
      i=i+1
    self.sigmaR = np.array(self.sigmaR, dtype=np.float64)    
    self.sigmaTheta = np.array(self.sigmaTheta, dtype=np.float64)
    self.sigmaRTheta = np.array(self.sigmaRTheta, dtype=np.float64)    

  def solveAndPrintConstraint(self):
    self.sigmax = np.linalg.solve(self.m, self.Gx) 
    self.sigmay= np.linalg.solve(self.m, self.Gy) 
    self.sigmaxy= np.linalg.solve(self.m, self.Gxy)
   
    self.computePolarConstraint()
    self.contraintes_n[0,:] = self.sigmax.transpose() 
    self.contraintes_n[1,:] = self.sigmay.transpose() 
    self.contraintes_n[2,:] = self.sigmaxy.transpose() 

    print("Contrainte maximale sigmax " + str(max(abs(self.sigmax/10**6))))
    print("Contrainte maximale sigmay " + str(max(abs(self.sigmay/10**6))))
    print("Contrainte maximale sigmaxy " + str(max(abs(self.sigmaxy/10**6))))

  def VonMisesConstraint(self):
    temp =(self.sigmax - self.sigmay)**2 + self.sigmaxy**2 + 6*(self.sigmaxy)**2
    res = 0.5*np.sqrt((temp))
    if max(res)> 6.42*10**6:
      print(f"{bcolors.WARNING}les contraintes appliqué sont superieur à la limite de rupture du materiaux{bcolors.ENDC}")
    