from parameter import *
import numpy as np


def openPreProcessFileNode(filename):
  t = open(filename, 'r')
  connection = np.loadtxt(filename, dtype=np.float64)[:,1:]
  t.close()

  return connection

def openPreProcessFile(filename):
  t = open(filename,'r')
  connection = np.loadtxt(filename, dtype=np.float64)
  t.close()

  return connection

def writeFile(filename, data):
  inputData = open(filename, 'w')

  for i in range (len(data)):
    inputData.write(str(data[i]))
  inputData.close()

def extractNode(f):
  lookup = 'nblock'

  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  noeuds = []
  lookup = '-1'

  for num, line in enumerate(f, 1):
    if '-1' in line:
      if(line.find('1') == 1):
        break
    noeuds.append(line)

  return noeuds

def extractConnection(f):
  lookup = 'eblock'

  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  matriceConnection = []
  lookup = '-1'

  for num, line in enumerate(f, 1):
    if '-1' in line:
      if(line.find('1') == 1):
        break
    matriceConnection.append(line)

  return matriceConnection

def extractDirichletX(f):
  lookup = 'Frictionless Supports X'

  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  f.readline()
  dirichletX = []
  temp = []
  lookup = 'cmsel'

  for num, line in enumerate(f, 1):
    if lookup in line:
      break
    temp.append(line)
  tempString = ""

  for i in range(0,num-1):
    if(i == num-2):
      tempString = tempString +temp[i]
    else :
      tempString = tempString +temp[i].rstrip()   
  dirichletX.append(tempString)

  return dirichletX

def extractDirichletY(f):
  lookup = 'Frictionless Supports Y'

  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  f.readline()
  dirichlet = []
  temp = []
  lookup = 'cmsel'

  for num, line in enumerate(f, 1):
    if lookup in line:
      break
    temp.append(line)
  tempString = ""

  for i in range(0,num-1):
    if(i == num-2):
      tempString = tempString +temp[i]
    else :
      tempString = tempString +temp[i].rstrip()   
  dirichlet.append(tempString)

  return dirichlet

def extractContour(f):
  lookup = 'Define Pressure Using Surface Effect Elements'

  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  f.readline()
  f.readline()
  f.readline()
  matriceConvection = []
  lookup = '-1'

  for num, line in enumerate(f, 1):
    if '-1' in line:
      if(line.find('1') == 1):
        break
    matriceConvection.append(line)

  return matriceConvection

def preProcessData(filename):
  filename0 = "noeuds.txt"
  filename1 = "connection.txt"
  filename2 = "cauchy.txt"
  filename3 = "dirichletX.txt"
  filename4 = "dirichletY.txt"

  inputData = open(filename,'r',encoding = "ISO-8859-1")
  noeuds = extractNode(inputData)
  matriceConnection = extractConnection(inputData)
  dirichletX = extractDirichletX(inputData)
  dirichletY = extractDirichletY(inputData)
  matriceConvection = extractContour(inputData)
  inputData.close()

  writeFile(filename0, noeuds)
  writeFile(filename1, matriceConnection)
  writeFile(filename2, matriceConvection)
  writeFile(filename3, dirichletX)
  writeFile(filename4, dirichletY)

def extractPreProcessData():
  filename0 = "noeuds.txt"
  filename1 = "connection.txt"
  filename2 = "cauchy.txt"
  filename3 = "dirichletX.txt"
  filename4 = "dirichletY.txt"

  node = openPreProcessFileNode(filename0)
  connection = openPreProcessFile(filename1)
  cauchy = openPreProcessFile(filename2)
  dirichletX = openPreProcessFile(filename3)
  dirichletY = openPreProcessFile(filename4)
  
  return node , connection, cauchy, dirichletX, dirichletY

def decorrelateDirichlet(dirichletTemp, node):
  seuil = pow(10,-18)
  dirichletX = [] 
  dirichletY = []

  for i in range(0, dirichletTemp.shape[0]):
    temp = int(dirichletTemp[i]-1)
    coord = node[temp,::1]
    if(coord[0] <= seuil) :
      dirichletX.append(temp)
    elif(coord[1] <= seuil):
      dirichletY.append(temp)
  dirichletX = np.array(dirichletX)
  dirichletY = np.array(dirichletY)

  return dirichletX, dirichletY

def applyDirichlet(dirichlet):
  temp = dirichlet.shape[0]
  T_Const = np.zeros((temp, 2))

  for i in range(temp):
    T_Const[i] = dirichlet[i]-1, D0

  return T_Const

def conditionLimite(dirX, dirY):
  # dirX, dirY = decorrelateDirichlet(dirichletTemp,node)
  dirichletX = applyDirichlet(dirX)
  dirichletY = applyDirichlet(dirY)

  return dirichletX, dirichletY

def sortMatriceConnectQ8(matConnec,sizeConnec,sizeBorder):
  shape = matConnec.shape
  matriceConnect = np.zeros((shape[0],shape[1]))

  for i in range(0, sizeConnec):
    matriceConnect[i,0] = matConnec[i,0]
    matriceConnect[i,1] = matConnec[i,4]
    matriceConnect[i,2] = matConnec[i,1]
    matriceConnect[i,3] = matConnec[i,5]
    matriceConnect[i,4] = matConnec[i,2]
    matriceConnect[i,5] = matConnec[i,6]
    matriceConnect[i,6] = matConnec[i,3]
    matriceConnect[i,7] = matConnec[i,7] 

  for i in range(sizeConnec, sizeConnec+sizeBorder):
    matriceConnect[i,0] = matConnec[i,0]
    matriceConnect[i,1] = matConnec[i,2]
    matriceConnect[i,2] = matConnec[i,1]

  return matriceConnect

def sortMatriceConnectT6(matConnec,sizeConnec,sizeBorder):
  shape = matConnec.shape
  matriceConnect = np.zeros((shape[0],shape[1]-2))
  
  for i in range(0, sizeConnec):
    matriceConnect[i,0] = matConnec[i,0]
    matriceConnect[i,1] = matConnec[i,4]
    matriceConnect[i,2] = matConnec[i,1]
    matriceConnect[i,3] = matConnec[i,5]
    matriceConnect[i,4] = matConnec[i,6]
    matriceConnect[i,5] = matConnec[i,7]
    

  for i in range(sizeConnec, sizeConnec+sizeBorder):
    matriceConnect[i,0] = matConnec[i,0]
    matriceConnect[i,1] = matConnec[i,2]
    matriceConnect[i,2] = matConnec[i,1]

  return matriceConnect

def assembleMatriceConnection2D(connection, convection):
  #concatenation des matrices connection et convection
  flag = -1
  matConnec = np.zeros((connection.shape[0]+convection.shape[0], connection.shape[1]-11))
  for i in range(connection.shape[0]):
    #pour les quad lin en Q4
    if(connection[i,8] == 4 and connection[i,13] != connection[i,14]):
      matConnec[int(connection[i,10]-1), 0::] = connection[i,11::]-1
    #pour les quad quadratique incomplet Q8
    if(connection[i,8] == 8 and connection[i,13] != connection[i,14]):
      flag = 0
      matConnec[int(connection[i,10]-1), 0::] = connection[i,11::]-1
    #pour les triangle lin T3
    if(connection[i,8] == 4 and connection[i,13] == connection[i,14]):
      matConnec[int(connection[i,10]-1), 0::] = connection[i,11::]-1
    #pour les triangle quadratique T6
    if(connection[i,8] == 8 and connection[i,13] == connection[i,14]):
      flag = 1
      matConnec[int(connection[i,10]-1), 0::] = connection[i,11::]-1

 
  if(convection.shape[1] == 8):  #pour ligne interpolation quadratique
    for i in range(convection.shape[0]):
      matConnec[int(convection[i,0]-1), 0:3] = convection[i,5::]-1
  
  if(convection.shape[1] == 7): #pour ligne interpolation lineaire
    for i in range(convection.shape[0]):
      matConnec[int(convection[i,0]-1), 0:2] = convection[i,5::]-1

  if(flag == 0):
    temp = sortMatriceConnectQ8(matConnec,connection.shape[0],convection.shape[0])
  elif(flag == 1): 
    temp = sortMatriceConnectT6(matConnec,connection.shape[0],convection.shape[0])

  return matConnec, temp