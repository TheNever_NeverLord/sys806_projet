import os


class bcolors:
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKCYAN = '\033[96m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'

def clear(): 
  if os.name == 'nt': 
    _ = os.system('cls')  
  else: 
    _ = os.system('clear') 

def get_program_parameters():
  import argparse
  description = 'Read and pre-process Ansys data.'
  epilogue = ''' '''
  parser = argparse.ArgumentParser(description=description, epilog=epilogue)
  parser.add_argument('mesh', help=f"{bcolors.WARNING}Required element geometry e.g T3 or Q4{bcolors.ENDC}")
  parser.add_argument('errL2', help=f"{bcolors.WARNING}Required L2 error calculation e.g true or false{bcolors.ENDC}")
  parser.add_argument('filename', help=f"{bcolors.WARNING}A required filename e.g mesh.dat. or None if L2 error is set to true{bcolors.ENDC}")
  args = parser.parse_args()
  return args.filename, args.errL2, args.mesh


def resolveproblem(fem):
  fem.computeInternalNodes()
  fem.computeCauchyBoundaryConditionNodes()
  fem.dirichletBoundaryConditions()
  fem.solve()
  fem.computeResidue()
  fem.computeConstraint()
  fem.solveAndPrintConstraint()
  return fem    