from natsort import os_sorted
from errorL2 import errorL2Q8, errorL2T6, processFem, processFem2
from exportSolution import export_sol_vtk
from subprocess import run
import os
from commun import bcolors
Flags = True


def elementMenuQ8():
  filenameList = os_sorted(os.listdir("../maillage/Quadrangles"))

  for i in range (0, len(filenameList)):
    print(f"{bcolors.OKCYAN}~/planeStress/meshType/Quadrangles >{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
  print(f"{bcolors.OKCYAN}~/planeStress/meshType/Quadrangles >{bcolors.ENDC}{bcolors.BOLD} r : Retour")
  print(f"{bcolors.OKCYAN}~/planeStress/meshType/Quadrangles >{bcolors.ENDC}{bcolors.BOLD} q : Quitter")
  k = True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStress/meshType/Quadrangles >{bcolors.WARNING}")
    digit = cmd.strip().isdigit()
    if(digit):
      if(int(cmd)>len(filenameList)-1):
        print(f"{bcolors.FAIL}entrer un nombre entre 0 et",len(filenameList)-1, f"{bcolors.ENDC}")
      else:
        print(f"{bcolors.ENDC}{bcolors.BOLD}")
        filename = filenameList[int(cmd)]
        fem = processFem("../maillage/Quadrangles/"+filename, False)
        export_sol_vtk(fem.mesh, fem.u, fem.v, fem.sigmax/10**6, fem.sigmay/10**6, fem.sigmaxy/10**6, fem.sigmaR/10**6, fem.sigmaTheta/10**6, fem.sigmaRTheta/10**6)
        if(os.name == 'posix'):
          os.system("./script.sh")
        elif(os.name == "nt"):
          run(["powershell", "-Command", "./script.ps1"], capture_output=True)
    else:
      if(cmd == 'r'):
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 1: T6")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} r: Retour")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" )
        k =False
      elif(cmd == 'q'):
        Flags = False
      elif(cmd == '?'):
        for i in range (0, len(filenameList)):
          print(f"{bcolors.OKCYAN}~/planeStress/meshType/Quadrangles >{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
        print(f"{bcolors.OKCYAN}~/planeStress/meshType/Quadrangles >{bcolors.ENDC}{bcolors.BOLD} r : Retour")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType/Quadrangles >{bcolors.ENDC}{bcolors.BOLD} q : Quitter")

def elementMenuT6():
  filenameList = os_sorted(os.listdir("../maillage/Triangles"))

  for i in range (0, len(filenameList)):
    print(f"{bcolors.OKCYAN}~/planeStress/meshType/Triangles >{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
  print(f"{bcolors.OKCYAN}~/planeStress/meshType/Triangles >{bcolors.ENDC}{bcolors.BOLD} r : Retour")
  print(f"{bcolors.OKCYAN}~/planeStress/meshType/Triangles >{bcolors.ENDC}{bcolors.BOLD} q : Quitter")
  k = True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStress/meshType/Triangles >{bcolors.WARNING}")
    digit = cmd.strip().isdigit()
    if(digit):
      if(int(cmd)>len(filenameList)-1):
        print(f"{bcolors.FAIL}entrer un nombre entre 0 et",len(filenameList)-1, f"{bcolors.ENDC}")
      else:
        print(f"{bcolors.ENDC}{bcolors.BOLD}")
        filename = filenameList[int(cmd)]
        fem = processFem("../maillage/Triangles/"+filename, True)
        export_sol_vtk(fem.mesh, fem.u, fem.v, fem.sigmax/10**6, fem.sigmay/10**6, fem.sigmaxy/10**6, fem.sigmaR/10**6, fem.sigmaTheta/10**6, fem.sigmaRTheta/10**6)
        if(os.name == 'posix'):
          os.system("./script.sh")
        elif(os.name == "nt"):
          run(["powershell", "-Command", "./script.ps1"], capture_output=True)
    else:
      if(cmd == 'r'):
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 1: T6")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} r: Retour")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" )
        k= False
      elif(cmd == 'q'):
        Flags = False
      elif(cmd == '?'):
        for i in range (0, len(filenameList)):
          print(f"{bcolors.OKCYAN}~/planeStress/meshType/Triangles >{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
        print(f"{bcolors.OKCYAN}~/planeStress/meshType/Triangles >{bcolors.ENDC}{bcolors.BOLD} r : Retour")
        print(f"{bcolors.OKCYAN}~/planeStress/meshType/Triangles >{bcolors.ENDC}{bcolors.BOLD} q : Quitter")

def meshTypeMenuL2():
  print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} 1: T6")
  print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
  print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} r: Retour") 
  print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" ) 
  k= True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStrain/L2Analisys/meshType >{bcolors.WARNING}")
    if(cmd == '1'):
      print(f"{bcolors.ENDC}{bcolors.BOLD}")
      errorL2T6()
    elif(cmd == '2'):
      print(f"{bcolors.ENDC}{bcolors.BOLD}")
      errorL2Q8()
    elif(cmd == 'r'):
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} 1: Résolution sur un maillage")
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} 2: Analyse des convergences des déplacements et des contraintes")
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} r: Retour")
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" )  
      k =False
    elif(cmd == 'q'):
      Flags = False
    elif(cmd == '?'):
      print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} 1: T6")
      print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
      print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} r: Retour") 
      print(f"{bcolors.OKCYAN}~/planeStress/L2Analisys/meshType >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" ) 

def meshTypeMenu():
  print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 1: T6")
  print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
  print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} r: Retour")
  print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" ) 
  k= True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.WARNING}")
    if(cmd == '1'):
      elementMenuT6()
    elif(cmd == '2'):
      elementMenuQ8()
    elif(cmd == 'r'):
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} 1: Résolution sur un maillage")
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} 2: Analyse des convergences des déplacements et des contraintes")
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} r: Retour")
      print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" ) 
      k =False
    elif(cmd == 'q'):
      Flags = False
    elif(cmd == '?'):
      print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 1: T6")
      print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
      print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} r: Retour")
      print(f"{bcolors.OKCYAN}~/planeStress/meshType >{bcolors.ENDC}{bcolors.BOLD} q: Quitter" ) 

def planeStressMenu():
  print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} 1: Résolution sur un maillage")
  print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} 2: Analyse des convergences des déplacements et des contraintes")
  print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} r: Retour")
  print(f"{bcolors.OKCYAN}~/planeStress >{bcolors.ENDC}{bcolors.BOLD} q: Quitter") 
  k= True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStress >{bcolors.WARNING}")
    if(cmd == '1'):
      meshTypeMenu()
      pass
    elif(cmd == '2'):
      meshTypeMenuL2()
      pass
    elif(cmd == 'r'):
      print(f"{bcolors.OKCYAN}~ >{bcolors.ENDC}{bcolors.BOLD} 1: Contraintes planes")
      print(f"{bcolors.OKCYAN}~ >{bcolors.ENDC}{bcolors.BOLD} 2: Déformations planes")
      print(f"{bcolors.OKCYAN}~ >{bcolors.ENDC}{bcolors.BOLD} 3: Quitter")
      k =False
    elif(cmd == 'q'):
      Flags = False
    elif(cmd == '?'):
      print(f"{bcolors.OKCYAN}~/planeStress>{bcolors.ENDC}{bcolors.BOLD} 1: Résolution sur un maillage")
      print(f"{bcolors.OKCYAN}~/planeStress>{bcolors.ENDC}{bcolors.BOLD} 2: Analyse des convergences des déplacements et des contraintes")
      print(f"{bcolors.OKCYAN}~/planeStress>{bcolors.ENDC}{bcolors.BOLD} q: Quitter") 


def elementsMenuQ8():
  filenameList = os_sorted(os.listdir("../maillage/Quadrangles"))

  for i in range (0, len(filenameList)):
    print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Quadrangles>{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Quadrangles>{bcolors.ENDC}{bcolors.BOLD} r : Retour")
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Quadrangles>{bcolors.ENDC}{bcolors.BOLD} q : Quitter")
  k = True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStrain/meshType/Quadrangles>{bcolors.WARNING}")
    digit = cmd.strip().isdigit()
    if(digit):
      if(int(cmd)>len(filenameList)-1):
        print(f"{bcolors.FAIL}entrer un nombre entre 0 et",len(filenameList)-1, f"{bcolors.ENDC}")
      else:
        print(f"{bcolors.ENDC}{bcolors.BOLD}")
        filename = filenameList[int(cmd)]
        fem = processFem2("../maillage/Quadrangles/"+filename, False)
        export_sol_vtk(fem.mesh, fem.u, fem.v, fem.sigmax/10**6, fem.sigmay/10**6, fem.sigmaxy/10**6, fem.sigmaR/10**6, fem.sigmaTheta/10**6, fem.sigmaRTheta/10**6)
        fem.VonMisesConstraint()
        if(os.name == 'posix'):
          os.system("./script.sh")
        elif(os.name == "nt"):
          run(["powershell", "-Command", "./script.ps1"], capture_output=True)
    else:
      if(cmd == 'r'):
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 1: T6")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} r: Retour")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} q: Quitter" )
        k =False
      elif(cmd == 'q'):
        Flags = False
      elif(cmd == '?'):
        for i in range (0, len(filenameList)):
          print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Quadrangles>{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Quadrangles>{bcolors.ENDC}{bcolors.BOLD} r : Retour")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Quadrangles>{bcolors.ENDC}{bcolors.BOLD} q : Quitter")

def elementsMenuT6():
  filenameList = os_sorted(os.listdir("../maillage/Triangles"))
  
  for i in range (0, len(filenameList)):
    print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Triangles>{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Triangles>{bcolors.ENDC}{bcolors.BOLD} r : Retour")
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Triangles>{bcolors.ENDC}{bcolors.BOLD} q : Quitter")
  k = True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStrain/meshType/Triangles>{bcolors.WARNING}")
    digit = cmd.strip().isdigit()
    if(digit):
      if(int(cmd)>len(filenameList)-1):
        print(f"{bcolors.FAIL}entrer un nombre entre 0 et", len(filenameList)-1, f"{bcolors.ENDC}")
      else:
        print(f"{bcolors.ENDC}{bcolors.BOLD}")
        filename = filenameList[int(cmd)]
        fem = processFem2("../maillage/Triangles/"+filename, True)
        fem.VonMisesConstraint()
        export_sol_vtk(fem.mesh, fem.u, fem.v, fem.sigmax/10**6, fem.sigmay/10**6, fem.sigmaxy/10**6, fem.sigmaR/10**6, fem.sigmaTheta/10**6, fem.sigmaRTheta/10**6)
        if(os.name == 'posix'):
          os.system("./script.sh")
        elif(os.name == "nt"):
          run(["powershell", "-Command", "./script.ps1"], capture_output=True)
    else:
      if(cmd == 'r'):
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 1: T6")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} r: Retour")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} q: Quitter" )
        k= False
      elif(cmd == 'q'):
        Flags = False
      elif(cmd == '?'):
        for i in range (0, len(filenameList)):
          print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Triangles>{bcolors.ENDC}{bcolors.BOLD} "+str(i)+": ",filenameList[i])
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Triangles>{bcolors.ENDC}{bcolors.BOLD} r : Retour")
        print(f"{bcolors.OKCYAN}~/planeStrain/meshType/Triangles>{bcolors.ENDC}{bcolors.BOLD} q : Quitter")


def planeStrainMenu():
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 1: T6")
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} r: Retour")
  print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} q: Quitter" ) 
  k= True
  global Flags

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.WARNING}")
    if(cmd == '1'):
      elementsMenuT6()
    elif(cmd == '2'):
      elementsMenuQ8()
    elif(cmd == 'r'):
      print(f"{bcolors.OKCYAN}~>{bcolors.ENDC}{bcolors.BOLD} 1: Contraintes planes")
      print(f"{bcolors.OKCYAN}~>{bcolors.ENDC}{bcolors.BOLD} 2: Déformations planes")
      print(f"{bcolors.OKCYAN}~>{bcolors.ENDC}{bcolors.BOLD} q: Quitter")
      k =False
    elif(cmd == 'q'):
      Flags = False
    elif(cmd == '?'):
      print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 1: T6")
      print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} 2: Q8")
      print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} r: Retour")
      print(f"{bcolors.OKCYAN}~/planeStrain/meshType>{bcolors.ENDC}{bcolors.BOLD} q: Quitter") 

def menu():
  k =True
  global Flags
  print(f"{bcolors.OKCYAN}~ >{bcolors.ENDC}{bcolors.BOLD} 1: Contraintes planes")
  print(f"{bcolors.OKCYAN}~ >{bcolors.ENDC}{bcolors.BOLD} 2: Déformations planes")
  print(f"{bcolors.OKCYAN}~ >{bcolors.ENDC}{bcolors.BOLD} q: Quitter")

  while(k == True and Flags == True):
    cmd = input(f"{bcolors.OKCYAN}~ >{bcolors.WARNING}")
    if(cmd == '1'):
      planeStressMenu()
    elif(cmd == '2'):
      planeStrainMenu()
      pass
    elif(cmd == 'q'):
      k = False
      Flags = False
      pass
    elif(cmd == '?'):
      print(f"{bcolors.OKCYAN}~>{bcolors.ENDC}{bcolors.BOLD} 1: Contraintes planes")
      print(f"{bcolors.OKCYAN}~>{bcolors.ENDC}{bcolors.BOLD} 2: Déformations planes")
      print(f"{bcolors.OKCYAN}~>{bcolors.ENDC}{bcolors.BOLD} q: Quitter")