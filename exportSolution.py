from os import linesep as endl

def export_mesh(mesh):        
  filename = "mesh.vtk"
  with open(filename, "w") as f:
    f.write("# vtk DataFile Version 1.0" + endl)
    f.write("PlaneStress Mesh SYS806" + endl)
    f.write("ASCII" + endl)
    f.write("DATASET UNSTRUCTURED_GRID" + endl)
    f.write("POINTS " +  str(mesh.numberOfNodes) + " float" + endl)

    for i in range(0, mesh.numberOfNodes):
      f.write(str(mesh.nodes[i][0]) + " " + str(mesh.nodes[i][1]) + " " + str(mesh.nodes[i][2]) + endl)
    f.write(endl + "CELLS " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + " " + str(mesh.numberOfElement*(mesh.numberOfNodePerElement+1)+mesh.numberOfBorderElement*(mesh.numberOfNodePerBorderElement+1)) + endl)
    
    for i in range(0, mesh.numberOfElement):
      f.write(str(int(mesh.numberOfNodePerElement)) + " " ) 
      for j in range(mesh.numberOfNodePerElement):
          f.write(str(int(mesh.matriceExport[i][j])) + " ")
      f.write(endl)
    
    for i in range(0, mesh.numberOfBorderElement):
      f.write(str(int(mesh.numberOfNodePerBorderElement)) + " " ) 
      for j in range(mesh.numberOfNodePerBorderElement):
          f.write(str(int(mesh.matriceExport[i+mesh.numberOfElement][j])) + " ")
      f.write(endl)
    f.write(endl + "CELL_TYPES " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + endl)
    
    if(mesh.numberOfNodePerElement == 6):
      cellType_elem = 22; #Triangle quadratique
    elif(mesh.numberOfNodePerElement == 8):
      cellType_elem = 23; #Quadrangle quadratique
    else:
      raise Exception("ELEM CELL TYPE ERROR")
    if(mesh.numberOfNodePerBorderElement == 3):
      cellType_elem_contour=21; #Line quadquadratique
    else:
      raise Exception("ELEM CONTOUR CELL TYPE ERROR")
    
    for i in range(0,mesh.numberOfElement):
      f.write(str(cellType_elem) + endl)
    
    for i in range(0, mesh.numberOfBorderElement):
      f.write(str(cellType_elem_contour) + endl)

def export_sol_vtk(mesh, u, v, sigmax, sigmay, sigmaxy, sigmaR, sigmaTheta, sigmaRTheta):        
  filename = "mesh_sol.vtk"
  with open(filename, "w") as f:
    f.write("# vtk DataFile Version 1.0" + endl)
    f.write("Temperature SYS807" + endl)
    f.write("ASCII" + endl)
    f.write("DATASET UNSTRUCTURED_GRID" + endl)
    f.write("POINTS " +  str(mesh.numberOfNodes) + " float" + endl)
    for i in range(0,mesh.numberOfNodes):
      f.write(str(mesh.nodes[i][0]) + " " + str(mesh.nodes[i][1]) + " " + str(mesh.nodes[i][2]) + endl)

    f.write(endl + "CELLS " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + " " + str(mesh.numberOfElement*(mesh.numberOfNodePerElement+1)+mesh.numberOfBorderElement*(mesh.numberOfNodePerBorderElement+1)) + endl)
    
    for i in range(0, mesh.numberOfElement):
      f.write(str(mesh.numberOfNodePerElement) + " " ) 
      for j in range(mesh.numberOfNodePerElement):
          f.write(str(int(mesh.matriceExport[i][j])) + " ")
      f.write(endl)
    
    for i in range(0, mesh.numberOfBorderElement):
      f.write(str(int(mesh.numberOfNodePerBorderElement)) + " " ) 
      for j in range(int(mesh.numberOfNodePerBorderElement)):
          f.write(str(int(mesh.matriceExport[i+mesh.numberOfElement][j])) + " ")
      f.write(endl) 
    f.write(endl + "CELL_TYPES " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + endl)
    
    if(mesh.numberOfNodePerElement == 6):
      cellType_elem = 22; #Triangle
    elif(mesh.numberOfNodePerElement == 8):
      cellType_elem = 23; #Quadrangle
    else:
      raise Exception("ELEM CELL TYPE ERROR")
    if(mesh.numberOfNodePerBorderElement == 3):
      cellType_elem_contour = 21; #Line
    else:
      raise Exception("ELEM CONTOUR CELL TYPE ERROR")
    
    for i in range(0, mesh.numberOfElement):
      f.write(str(cellType_elem) + endl) 
    
    for i in range(0, mesh.numberOfBorderElement):
      f.write(str(cellType_elem_contour) + endl)

    f.write(endl + "POINT_DATA " + str(mesh.numberOfNodes) + endl)
    f.write("Vectors Deplacement_u_v_(m) float" + endl)
    for i in range(0, mesh.numberOfNodes):
      f.write(str(u[i])+" "+str(v[i])+" 0.0" + endl)

    # f.write("Scalars X float" + endl)
    # f.write("LOOKUP_TABLE default" + endl)
    # for i in range(0, mesh.numberOfNodes):
    #   f.write(str(u[i])+ endl)

    # f.write("Scalars Y float" + endl)
    # f.write("LOOKUP_TABLE default" + endl)
    # for i in range(0, mesh.numberOfNodes):
    #   f.write(str(v[i])+ endl)  

    f.write("Scalars SigmaX(MPa) float" + endl)
    f.write("LOOKUP_TABLE default" + endl)
    for i in range(0, mesh.numberOfNodes):
      f.write(str(sigmax[i]) + endl)

    f.write("Scalars SigmaY(MPa) float" + endl)
    f.write("LOOKUP_TABLE default" + endl)
    for i in range(0, mesh.numberOfNodes):
      f.write(str(sigmay[i]) + endl)

    f.write("Scalars SigmaXY(MPa) float" + endl)
    f.write("LOOKUP_TABLE default" + endl)
    for i in range(0, mesh.numberOfNodes):
      f.write(str(sigmaxy[i]) + endl)

    f.write("Scalars SigmaR(MPa) float" + endl)
    f.write("LOOKUP_TABLE default" + endl)
    for i in range(0, mesh.numberOfNodes):
      f.write(str(sigmaR[i]) + endl)

    f.write("Scalars SigmaTheta(MPa) float" + endl)
    f.write("LOOKUP_TABLE default" + endl)
    for i in range(0, mesh.numberOfNodes):
      f.write(str(sigmaTheta[i]) + endl)
